<!DOCTYPE HTML>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="image/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="image/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="image/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="image/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="image/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="image/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="image/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="image/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="image/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="image/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="image/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="image/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="image/favicon/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <title>MD Riajul Islam Riaj || Personal Portfolio</title>

  <!-- Styles -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i&display=swap" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" media="screen">
</head>

<body>
  <div class="animsition">
    <div class="loader">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>

    <!-- Content CLick Capture-->

    <div class="click-capture"></div>

    <!-- Sidebar Menu-->

    <div class="menu">
      <span class="close-menu right-boxed">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2em" height="2em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512" class="iconify" data-icon="ion:close-outline" data-inline="false" style="transform: rotate(360deg);">
          <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M368 368L144 144"></path>
          <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M368 144L144 368"></path>
        </svg>
      </span>
      <ul class="menu-list right-boxed">
        <li data-menuanchor="page1">
          <a href="#page1">Home</a>
        </li>
        <li data-menuanchor="page2">
          <a href="#page2">Specialization</a>
        </li>
        <li data-menuanchor="page3">
          <a href="#page3">Resume</a>
        </li>
        <li data-menuanchor="page4">
          <a href="#page4">About</a>
        </li>
        <li data-menuanchor="page5">
          <a href="#page5">Projects</a>
        </li>
        <!-- <li  data-menuanchor="page7">
          <a href="#page7">Testimonials</a>
        </li> -->
        <li data-menuanchor="page8">
          <a href="#page8">Contact</a>
        </li>
      </ul>
      <div class="menu-footer right-boxed">
        <div class="social-list">
          <a href="https://www.facebook.com/riazrabbi/" target="_blank" class="icon a">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" transform: rotate(360deg); preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512" class="iconify" data-icon="ion:logo-facebook" data-inline="false" width="2em" height="2em">
              <path d="M480 257.35c0-123.7-100.3-224-224-224s-224 100.3-224 224c0 111.8 81.9 204.47 189 221.29V322.12h-56.89v-64.77H221v-49.36c0-56.13 33.45-87.16 84.61-87.16c24.51 0 50.15 4.38 50.15 4.38v55.13H327.5c-27.81 0-36.51 17.26-36.51 35v42.02h62.12l-9.92 64.77h-52.2v156.53C398.1 461.85 480 369.18 480 257.35z" fill-rule="evenodd" clip-rule="evenodd" fill="currentColor"></path>
            </svg>
          </a>
          <a href="https://www.linkedin.com/in/md-riajul-islam-riaj-20b8a4191" target="_blank" class="icon b">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2em" height="2em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512" class="iconify" data-icon="ion:logo-linkedin" data-inline="false" style="transform: rotate(360deg);">
              <path d="M444.17 32H70.28C49.85 32 32 46.7 32 66.89v374.72C32 461.91 49.85 480 70.28 480h373.78c20.54 0 35.94-18.21 35.94-38.39V66.89C480.12 46.7 464.6 32 444.17 32zm-273.3 373.43h-64.18V205.88h64.18zM141 175.54h-.46c-20.54 0-33.84-15.29-33.84-34.43c0-19.49 13.65-34.42 34.65-34.42s33.85 14.82 34.31 34.42c-.01 19.14-13.31 34.43-34.66 34.43zm264.43 229.89h-64.18V296.32c0-26.14-9.34-44-32.56-44c-17.74 0-28.24 12-32.91 23.69c-1.75 4.2-2.22 9.92-2.22 15.76v113.66h-64.18V205.88h64.18v27.77c9.34-13.3 23.93-32.44 57.88-32.44c42.13 0 74 27.77 74 87.64z" fill="currentColor"></path>
            </svg>
          </a>
        </div>
        <div class="copy">&copy; 2019-<?php echo date("Y"); ?></div>
      </div>
    </div>