<!-- Navbar -->

    <header class="navbar navbar-fullpage boxed">
      <div class="navbar-bg"></div>
      <a class="brand" href="#">
        <!-- <img alt="" src="image/jd.png"> -->
        <div class="brand-info">
          <div class="brand-name">
            <!-- <i style="color: #ffb300;" src="image/icon/jd.ico" ></i> -->
            <img alt="" src="image/jd.png" height="45px">
          MD Riajul Islam Riaj
        </div>
        </div>
      </a>

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
      <div class="contacts d-none d-md-block">
        <div class="contact-item">
          +88 01777687972
        </div>
        <div class="contact-item spacer">
          /
        </div>
        <div class="contact-item">
          <a href="mailto:riaz.islam4542@gmail.com"><span class="__cf_email__" >Contact in Email</span></a>
        </div>
      </div>
    </header>