<div data-anchor="page2" class="pp-scrollable section section-2">
  <div class="scroll-wrap">
    <div class="scrollable-content">
      <div class="vertical-title text-white  d-none d-lg-block"><span>what I do</span></div>
      <div class="vertical-centred">
        <div class="boxed boxed-inner">
          <div class="boxed">
            <div class="container">
              <div class="intro">
                <h2 class="title mb-5 pb-5"> <span class="text-primary">My</span> specialization</h2>
                <div class="row-specialization row">
                  <div class="col-specialization col-md-6 col-lg-4">
                    <span class="icon-specialization icon">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2em" height="2em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                        <path d="M4 6v10h5v-4a2 2 0 0 1 2-2h5a2 2 0 0 1 2 2v4h2V6H4M0 20v-2h4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h4v2h-6a2 2 0 0 1-2 2h-5a2 2 0 0 1-2-2H0m11.5 0a.5.5 0 0 0-.5.5a.5.5 0 0 0 .5.5a.5.5 0 0 0 .5-.5a.5.5 0 0 0-.5-.5m4 0a.5.5 0 0 0-.5.5a.5.5 0 0 0 .5.5a.5.5 0 0 0 .5-.5a.5.5 0 0 0-.5-.5M13 20v1h1v-1h-1m-2-8v7h5v-7h-5z" fill="white" />
                      </svg>
                    </span>
                    <h4 class="text-uppercase">Web Design</h4>
                    <p>Responsive Website front-end design using HTML, CSS, JS including Research, Creative Idea, Standard Code format. Also used third party to build professional webpage design . </p>
                  </div>
                  <div class="col-specialization col-md-6 col-lg-4">
                    <span class="icon-specialization icon">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2em" height="2em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
                        <path d="M7 5h10v2h2V3c0-1.1-.9-1.99-2-1.99L7 1c-1.1 0-2 .9-2 2v4h2V5zm8.41 11.59L20 12l-4.59-4.59L14 8.83L17.17 12L14 15.17l1.41 1.42zM10 15.17L6.83 12L10 8.83L8.59 7.41L4 12l4.59 4.59L10 15.17zM17 19H7v-2H5v4c0 1.1.9 2 2 2h10c1.1 0 2-.9 2-2v-4h-2v2z" fill="white" />
                      </svg>
                    </span>
                    <h4 class="text-uppercase">Web Developmment</h4>
                    <p>Developing website using php framework. Currently learning Nodejs as it becomes highly recommended framework for todays world.</p>
                  </div>
                  <div class="col-specialization col-md-6 col-lg-4">
                    <span class="icon-specialization icon">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="2em" height="2em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 32 32">
                        <path d="M21.49 13.115l-9-5a1 1 0 0 0-1 0l-9 5A1.008 1.008 0 0 0 2 14v9.995a1 1 0 0 0 .52.87l9 5A1.004 1.004 0 0 0 12 30a1.056 1.056 0 0 0 .49-.135l9-5A.992.992 0 0 0 22 24V14a1.008 1.008 0 0 0-.51-.885zM11 27.295l-7-3.89v-7.72l7 3.89zm1-9.45L5.06 14L12 10.135l6.94 3.86zm8 5.56l-7 3.89v-7.72l7-3.89z" fill="white" />
                        <path d="M30 6h-4V2h-2v4h-4v2h4v4h2V8h4V6z" fill="white" />
                      </svg>
                    </span>
                    <h4 class="text-uppercase">Software Develop</h4>
                    <p>Developing a software using Java FXML/ Spring Boot.</p>
                  </div>
                </div>
                <div class="view-all">
                  <a href="resume.pdf" target="_blank">
                    Download resume
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>