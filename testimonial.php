<div data-anchor="page7" class="pp-scrollable text-white section section-7">
  <div class="scroll-wrap">
    <div class="section-bg" style="background-image:url(images/bg/reviews.jpg);"></div>
    <div class="bg-quote"></div>
    <div class="scrollable-content">
      <div class="vertical-title  d-none d-lg-block"><span>testimonials</span></div>
      <div class="vertical-centred">
        <div class="boxed boxed-inner">
          <div class="boxed">
            <div class="container">
              <div class="intro">
                <div class="review-carousel owl-carousel">
                  <div class="review-carousel-item">
                    <div class="review-row">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="review-author">
                            <div class="author-name">David & Elisa</div>
                            <i>Apartment view lake at Brooklyn</i>
                          </div>
                        </div>
                        <div class="col-md-7 text">
                          <p>If you are seeking an Interior designer that will understand exactly your needs, and someone who will utilise their creative and technical skills in parity with your taste, then Suzanne at The Ramsay Studio is perfect.</p>
                        </div>
                      </div>
                    </div>
                    <div class="review-row">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="review-author">
                            <div class="author-name">Amanda</div>
                            <i>Apartment view lake at Brooklyn</i>
                          </div>
                        </div>
                        <div class="col-md-7 text">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio dolorem reiciendis doloremque veniam perspiciatis quam velit pariatur eius, repellendus dolores eveniet maiores sed. Quod quam minus dolore sed cumque aliquid.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="review-carousel-item">
                    <div class="review-row">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="review-author">
                            <div class="author-name">David & Elisa</div>
                            <i>Apartment view lake at Brooklyn</i>
                          </div>
                        </div>
                        <div class="col-md-7 text">
                          <p>If you are seeking an Interior designer that will understand exactly your needs, and someone who will utilise their creative and technical skills in parity with your taste, then Suzanne at The Ramsay Studio is perfect.</p>
                        </div>
                      </div>
                    </div>
                    <div class="review-row">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="review-author">
                            <div class="author-name">Amanda</div>
                            <i>Apartment view lake at Brooklyn</i>
                          </div>
                        </div>
                        <div class="col-md-7 text">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio dolorem reiciendis doloremque veniam perspiciatis quam velit pariatur eius, repellendus dolores eveniet maiores sed. Quod quam minus dolore sed cumque aliquid.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="review-carousel-item">
                    <div class="review-row">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="review-author">
                            <div class="author-name">David & Elisa</div>
                            <i>Apartment view lake at Brooklyn</i>
                          </div>
                        </div>
                        <div class="col-md-7 text">
                          <p>If you are seeking an Interior designer that will understand exactly your needs, and someone who will utilise their creative and technical skills in parity with your taste, then Suzanne at The Ramsay Studio is perfect.</p>
                        </div>
                      </div>
                    </div>
                    <div class="review-row">
                      <div class="row">
                        <div class="col-md-5">
                          <div class="review-author">
                            <div class="author-name">Amanda</div>
                            <i>Apartment view lake at Brooklyn</i>
                          </div>
                        </div>
                        <div class="col-md-7 text">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio dolorem reiciendis doloremque veniam perspiciatis quam velit pariatur eius, repellendus dolores eveniet maiores sed. Quod quam minus dolore sed cumque aliquid.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>